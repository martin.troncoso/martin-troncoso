#!/bin/bash

args=( $@ )

echo "A continuación deberá ingresar datos. Por favor, ingrese la IP o el dominio al final"

for ((i=1;i<7;i++));do
        read -p "Ingrese dato a contemplar. Si considera que no debe ingresar más, presione enter: " valor
        args[$i]=$valor
done

for i in "${!args[@]}"; do
        if [[ ${args[i]} != "" ]]; then
                cont=$(( cont + 1 ))
        fi
done

if [[ ${args[-1]} =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]] || [[ ${args[-1]} == *"www"* ]]; then
        if [[ $cont -ge 2 ]]; then
               for opcion in "${!args[@]}"; do

                        case ${args[$opcion]} in
                                -C) if [[ ${args[$opcion]} =~ ^[0-9]+$ ]] && [[ ${args[$opcion]} != 0 ]]; then
                                        cantidad=${args[$(($opcion+1))]}
                                        counter="-c $cantidad"
                                    fi
                                ;;

                                -T) timestamp="-D"
                                ;;

                                -p) if [[ ${args[$opcion]} = 4 ]] || [[ ${args[$opcion]} = 6 ]]; then
                                        proto=${args[$(($opcion+1))]}
                                        p="-$proto"
                                    fi
                                ;;

                                -b) b="-b"
                                ;;

                                *) echo "ERROR. Opción incorrecta"}
                               ;;
                        esac
                done
 
				# ping $b $timestamp $p $counter ${args[-1]}
               ping ${args[*]}

        else
                echo "ERROR. Debe indicar al menos una opción más además de la IP o dominio"
        fi
else
        echo "ERROR. Debe ingresar IP o dominio"
fi
