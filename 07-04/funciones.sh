#!/bin/bash

mostrar_ayuda () {
        # Función que mostrará ayuda al usuario.

        # Se define variable local. Se espera recibir como parámetro $?
        local arg=$1

        echo -e "\n###########################################################################################################"
        echo "                                     Ayuda para ejecución de script                                        "
        echo "###########################################################################################################"
        echo -e "\nCon este script podrá ejecutar un ping, ingresando los siguientes argumentos:"
        echo -e "\t* Dirección IP, host o dominio."
        echo -e "\t* -c y un número entero positivo para indicar la cantidad de pings a ejecutar. Por ejemplo: -c 4"
        echo -e "\t* -T para opción timestamp."
        echo -e "\t* -p y un 4 o 6 para indicar el protocolo. Por ejemplo: -p 4"
        echo -e "\t* -b para Broadcast"
        echo -e "\nDebe ingresar obligatoriamente una dirección IP, host o dominio y al menos un argumento adicional.\n"

        if [ $arg -eq 0 ]; then
                exit 0
        else
                exit 1
        fi
}


verif_array () {
        # Función que permite verificar si el array enviado como argumento tiene elementos o está vacío.

        #Se definen variables locales. Se espera recibir como parámetro un array.
        local args=($@)

        if [ -z $args ]; then
                mostrar_ayuda 1
        fi
}


verif_ipdomhost () {
        # Función que permite verificar si el argumento recibido como parámetro es una dirección IP, un host o un dominio.

        # Se definen variables locales. Se espera recibir como parámetro el último elemento del array.
        local arg=$1
        local hostdom="^[a-zA-Z][a-zA-Z0-9\.\-]+$"
        local ipv4="^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$"

        # En primera instancia, se verifica si la variable arg (que contiene el argumento ingresado como parámetro) es una dirección ip, un host o un dominio.
if [[ $arg =~ $ipv4 ]]; then

                # Se analiza si la dirección IP cuenta con un formato válido.
                # Se guarda IFS en una nueva variable y se redefine con un punto. Se guarda variable en un array.
                oldifs=$IFS
                IFS="."
                ipcompleta=($arg)

                # Se recorre el vector. Si cada octeto es menor a 255, se valida la ip.
                for octeto in ${!ipcompleta[@]}; do
                        if [[ ${ipcompleta[octeto]} -le 255 ]]; then
                                continue
                                return 0
                        else
                                echo "IP ingresada no es correcta. Por favor, intente nuevamente."
                                mostrar_ayuda 1
                        fi
                done

                # Se restaura valor de IFS.
                IFS=$oldifs

        elif [[ $arg =~ $hostdom ]]; then
                # Ingresó host o dominio válido
                return 0

        elif [ $arg == "--help" ]; then
               mostrar_ayuda 0
        else
                # En cualquier otro caso, se devuelve mensaje de error y finaliza.
                echo "El argumento ingresado no es válido. Por favor, ejecute nuevamente el script indicando una IP, host o dominio válido."
                mostrar_ayuda 1
        fi
}


dos_args() {
        # Función que valida que se estén considerando al menos dos argumentos.

        # Se define variable local. Se espera recibir como parámetro un array.
        local args=($@)

        # Si la cantidad de elementos del array no es mayor o igual a 2, devuelve mensaje de error y finaliza.
        if [[ ! "${#args[@]}" -ge 2 ]]; then
                echo "Debe ingresar al menos un argumento más, además de la ip, host o dominio. Por favor, intente nuevamente."
                mostrar_ayuda 1
        else
                return 0
        fi
}


valid_numpos () {
        # Esta función valida si el valor enviado por parámetro es un número y si es un entero positivo.

        # Se definen variables locales. Se espera recibir como parámetro un valor.
        local arg=$1

        # Si lo enviado por argumento es un número y es mayor a 0, en la variable counter se guarda -c y el valor enviado como argumento. Caso contraio, muestra mensaje de error y finaliza.
        if [[ $arg == $digito ]] && [[ $arg -gt 0 ]]; then
                counter="-c $1"
                return 0
        else
                echo "Argumento inválido. Debe ingresar un número entero positivo luego de -c."
                mostrar_ayuda 1
        fi

}


valid_prot () {
        # Esta función valida si el valor enviado por parámetro es un número y si es un 4 o un 6.

        # Se definen variables locales. Se espera recibir como parámetro un valor.
        local arg=$1

        # Si lo enviado por argumento es un número y es 4 o 6, en la variable p se guarda - y el valor enviado como argumento.
        if [[ $arg == $digito ]] && ([[ $arg -eq 4 ]] || [[ $arg -eq 6 ]]); then
                p="-$1"
                return 0
        else
                echo "Argumento inválido. Debe ingresar un número 4 ó 6 luego de -p."
                mostrar_ayuda 1
        fi
}

