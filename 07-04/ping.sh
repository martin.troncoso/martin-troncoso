#!/bin/bash

# Se definen variables que se utilizarán en el script.
args=($@)
digito="[[:digit:]]"
hostdom="^[a-zA-Z][a-zA-Z0-9\.\-]+$"
ipv4="^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$"

# En primera instancia, se verifican que se hayan ingresado argumentos.
if [ "${#args[@]}" -eq 0 ]; then
        echo "Por favor, ingrese argumentos válidos. Puede verificar la ayuda ingresando --help"
        exit 1
fi


# Luego, se verifica si el último argumento es una dirección ip o un host o un dominio (o la ayuda)
if [[ "${args[-1]}" =~ $ipv4 ]]; then

        # Se analiza si la dirección IP cuenta con un formato válido.
        # Se guarda IFS en una nueva variable y se redefine con un punto. Se guarda último argumento en un array.
        oldifs=$IFS
        IFS="."
        ipcompleta=(${args[-1]})

        # Se recorre el vector. Si cada octeto es menor a 255, se valida la ip.
        for octeto in ${!ipcompleta[@]}; do
                if [[ ${ipcompleta[octeto]} -le 255 ]]; then
                        continue
                else
                        echo "IP ingresada no es correcta. Por favor, intente nuevamente."
                        exit 1
                fi
        done

        # Se restaura valor de IFS.
        IFS=$oldifs

elif [[ "${args[-1]}" =~ $hostdom ]]; then
        # Ingresó host o dominio válido
        echo "Se valida formato de host o dominio. Continúa script."

elif [ "${args[-1]}" == "--help" ]; then
        echo "###########################################################################################################"
        echo "                                     Ayuda para ejecución de script                                        "
        echo "###########################################################################################################"
        echo -e "\nCon este script podrá ejecutar un ping, ingresando los siguientes argumentos:"
        echo -e "\t* Dirección IP, host o dominio."
        echo -e "\t* -c y un número entero positivo para indicar la cantidad de pings a ejecutar. Por ejemplo: -c 4"
        echo -e "\t* -T para opción timestamp."
        echo -e "\t* -p y un 4 o 6 para indicar el protocolo. Por ejemplo: -p 4"
        echo -e "\t* -b para Broadcast"
        echo -e "\nDebe ingresar obligatoriamente una dirección IP, host o dominio y al menos un argumento adicional.\n"
        exit 0
else
        # En cualquier otro caso, se devuelve mensaje de error y finaliza el script.
        echo "El argumento ingresado no es válido. Por favor, ejecute nuevamente el script indicando una IP, host o dominio válido."
        exit 1
fi

# Ya sabiendo que el último argumento es una IP, host o dominio válido, se verifica que haya al menos un argumento más. Si la cantidad de elementos del array
   # no es mayor o igual a 2, devuelve mensaje de error y el script finaliza.
if [[ ! "${#args[@]}" -ge 2 ]]; then
        echo "Debe ingresar al menos un argumento más, además de la ip, host o dominio. Por favor, intente nuevamente."
        exit 1
fi

# Se recorre el vector
for opcion in "${!args[@]}"; do

        # Se valida cada elemento ingresado.
        case ${args[$opcion]} in

                # Valida que el próximo argumento sea un número y entero positivo. De ser así, se guarda ese argumento en la variable cantidad y se conforma la variable counter con -c y ese valor.
                -C|-c)
                        if [[ ${args[$(($opcion+1))]} =~ $digito ]] && [[ ${args[$(($opcion+1))]} -gt 0 ]]; then
                                cantidad=${args[$(($opcion+1))]}
                                counter="-c $cantidad"
                        else
                                echo "Argumento inválido. Debe ingresar un número entero positivo luego de -c."
                                exit 1
                        fi
                        ;;

                # Si el argumento ingresado es -T, se guarda -D en la variable timestamp.
                -T)
                        timestamp="-D"
                        ;;

                #Si el argumento ingresado es -p, valida que el argumento siguiente sea un 4 o un 6. En ese caso, se guarda dicho argumento en la variable proto y -ese valor en p.
		-p)
			if [[ ${args[$(($opcion+1))]} =~ $digito ]] && ([[ ${args[$((opcion+1))]} -eq 4 ]] || [[ ${args[$((opcion+1))]} -eq 6 ]]); then
                                proto=${args[$(($opcion+1))]}
                                p="-$proto"
                        else
                                echo "Argumento inválido. Debe ingresar un número 4 ó 6 luego de -p."
                                exit 1
                        fi
                        ;;

                 # Si el argumento ingresado es -b, se guarda dicho valor en la variable b.
                 -b)
                         b="-b"
                        ;;

                # Para cualquier otro argumento, se verifica que no se trate del último elemento o no que sea un número.
                # Caso contrario, devuelve mensaje de error y finaliza script.
                *)
			if [[ "${args[$opcion]}" == "${args[-1]}" ]] || [[ "${args[$opcion]}" =~ $digito ]]; then
				continue
			else
				echo "ERROR. Opción ${args[$opcion]} incorrecta. Intente nuevamente."
				exit 1
                        fi
			;;
	esac

done

ping $b $timestamp $p $counter ${args[-1]}
