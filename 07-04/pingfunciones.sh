#!/bin/bash

# Script con funciones
source funciones.sh

# Se define vector en el que se alojarán los argumentos ingresados y variable global.
args=($@)
digito="[[:digit:]]"

# Se llama a la función verif_array para verificar que se hayan ingresado argumentos.
verif_array "${args[@]}"

# Se llama a la función verif_ipdomhost para verificar si el último argumento ingresado (enviado como parámetro) es una dirección IP, dominio o host.
verif_ipdomhost "${args[-1]}"

# Una vez verificado esto, se llama a la función dos_args para asegurar que se haya ingresado al menos un argumento más.
dos_args "${args[@]}"

# Se recorre el vector
for opcion in "${!args[@]}"; do

        # Se valida cada elemento ingresado.
        case ${args[$opcion]} in

                # Si el argumento ingresado es --help, se llama a la función mostrar_ayuda.
                --help)
                        mostrar_ayuda 0
                        ;;

                # Si el argumento ingresado es -C o -C, se guarda el próximo argumento en la variable cantidad y se llama a función valid_numpos para validar si se trata de un número
                        # entero positivo.
                -C|-c)
                        cantidad=${args[$(($opcion+1))]}
                        valid_numpos $cantidad
                        ;;

                # Si el argumento ingresado es -T, se guarda -D en la variable timestamp.
                -T)
                        timestamp="-D"
                        ;;

                # Si el argumento ingresado es -p, se guarda el próximo argumento en la variable proto y se llama a la función valid_prot para validar si se trata de un número 4 o 6.
                -p)
                        proto=${args[$(($opcion+1))]}
                        valid_prot $proto
                        ;;

                # Si el argumento ingresado es -b, se guarda dicho valor en la variable b.
                 -b)
                         b="-b"
                        ;;

                # Para cualquier otro valor, se llama a la función valid_ult enviando el array como parámetro.
                *)
                        if [[ "${args[$opcion]}" == "${args[-1]}" ]] || [[ "${args[$opcion]}" =~ $digito ]]; then
                                continue
                        else
                                echo "ERROR. Opción ${args[$opcion]} incorrecta. Intente nuevamente."
                                mostrar_ayuda 1
                        fi
                        ;;

        esac
done

# Se ejecuta ping con valores de las variables.
ping $b $timestamp $p $counter ${args[-1]}

