#!/bin/bash

# Se define función help
function help(){
	echo "###########################################################################################################"
	echo "                                     Ayuda para ejecución de script                                        "
	echo "###########################################################################################################"
	echo -e "\nCon este script podrá activar o desactivar repositorios, así como también instalar ficheros deb."
	echo "- Para activar repositorios, debe indicar un 1. Para desactivarlos, un 0."
        echo "  Luego indicar el componente: main, contrib, universe, multiverse, restricted o non-free."	
        echo "  Por ejemplo: 1 main activará los repositorios para main. 0 main los desactiva."	
	echo "- Para instalar un fichero deb, debe ingresarlo como argumento."
	echo "Debe ingresar al menos un argumento válido."	
	exit 0
}

# Se define función valid, que validará si el argumento es uno de los permitidos. En caso contrario, finaliza.
function valid(){
	if [ "$1" == "main" ] || [ "$1" == "contrib" ] || [ $1 == 'universe' ] || [ $1 == 'multiverse' ] || [ $1 == 'restricted' ] || [ $1 == 'non-free' ]; then
		echo " "
	else	
		echo "Argumento no válido. Intente nuevamente."
		exit 1
	fi
}

# Se define función repos.
function repost(){
        # Se setea el IFS (Internal Field Separator) sólo para saltos de línea.
        IFS=$'\n'

        # Se recorren las líneas del archivo. Las que inician con deb se guardan en un array.
        while IFS= read -r linea
        do
                if [[ $linea =~ ^deb* ]]; then
                        array=("${array[@]}" $linea)
                fi
        done < /etc/apt/sources.list

        # Se vuelve a setear el IFS como estaba.
        IFS=$' \t\n'

	# Se verifica si el argumento es 1 (Activación) o 0 (Desactivación)
        case $1 in
		1) #Activación
			#Se recorre el vector. Si en la línea está el argumento (por ejemplo, main), devuelve mensaje. 
			# En caso de que no, agrega el argumento al finalizar la línea en el elemento del vector y luego ello se incorpora en el
			  # fichero /etc/apt/sources.list. Se devuelve mensaje y se actualiza.
        	        for lin in "${!array[@]}"; do
                		if [[ "${array[lin]}" =~ $2 ]]; then
                        		echo "Configuración $2 existente"
                        	else
                                	array[lin]="${array[lin]} $2"
					echo "${array[lin]}" >> /etc/apt/sources.list
					echo "Configuración $2 incorporada"
					apt update && apt upgrade
				fi
                	done
			;;

		0) #Desactivación
			# Se recorre el vector. Si en la línea está el argumento (por ejemplo, main), se elimina dicho componente de la línea´.
			# Se devuelve mensaje y se actualiza.	
			for lin in "${!array[@]}"; do
                                if [[ "${array[lin]}" =~ $2 ]]; then
                                        sed -i 's/'" $2"'//g' /etc/apt/sources.list
					echo "Configuración $2 desactivada"
					apt update && apt upgrade
                                fi
                        done
                        ;;

		*) # Cualquier otra opción.
			echo "Ingrese un argumento válido."
			exit 1
	esac
}      

# Se define función instalación
function instalacion(){
	
	# Se conforma la ruta junto al fichero ingresado como argumento.
	ruta="var/cache/apt/archives/$1"

	#Se instala fichero deb
	dpkg -i $ruta 2>/dev/null

	# Se analiza respuesta.
	case $? in
		1) # Error
        		# Se visualiza porción de salida. Se identifican dependencias.
			salida=$(dpkg-deb -I $ruta | sed -n '/Depends/,/Suggest/p')
			echo -e "Error al instalar. Verificar dependencias. Ellas son:\n $salida"
        		echo "Por favor, descargue las dependencias e instale utilizando este script."
        		exit 1
			;;
		
		2) # Error
			echo "Fichero incorrecto. Verifique nomenclatura." 
			exit 1
			;;
		
		0) # Éxito
        		echo "Instalación correcta."
			exit 0
			;;
		
		*) # Otra opción
			echo "Verificar error."
			exit 1
			;;
	esac
}

# Se verifican argumentos.
case $1 in
	
	# Si es --help, --HELP o --Help, se llama a la función help.
	--help|--HELP|--Help) 
		help
	;;
	
	# Si finaliza con .deb, se llama a la función instalación, enviando el fichero como parámetro.
	*.deb) 
		instalacion $1
	;;
	
	# Si es 1 o 0, se llama a la función valid para verificar el segundo argumento y luego a la función repost con los argumentos para
	 # efectuar las configuraciones.
	1|0) #Activar / Desactivar
		valid $2
		repost $1 $2	
	;;
	
	*) # Argumento incorrecto
		echo "Argumento incorrecto. Intente nuevamente."
		exit 1
		;;
esac
