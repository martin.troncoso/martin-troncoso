#!/bin/bash

# Se declaran e inicializan variables que se utilizarán.
args=( $@ )
flag=0
opciones=''
directorio=''
frecuencia='50 23 * * 6'

# Se define la función help, que contiene la ayuda para el usuario que ejecutará el script.

function help(){
        echo "#########################################################################"
        echo "                     # Ayuda para ejecutar script #                      "
        echo -e "#########################################################################\n"
        echo -e "Con este script podrá efectuar un backup y programar su automatización.\n"
        echo "Para ejecutar el script indique como argumentos: "
        echo " - Opciones (por ejemplo, -avzh),"
        echo " - Directorio a backupear (por ejemplo: dir/)"
        echo " - Frecuencia, considerando entre las siguientes:"
        echo "      1     Una vez por día, todos los días, a las 23:50 hs."
        echo "      2     Una vez por día, todos los días, a las 00:10 hs."
        echo "      3     Una vez por día, de lunes a viernes, a las 23:50 hs."
        echo "      4     Una vez por día, de lunes a viernes, a las 00:10 hs."
        echo "      5     Dos veces por día, todos los días, a las 11:50 y 23:50 hs."
        echo -e "De esta manera, podría, por ejemplo, ejecutar: /.backup.sh -avzh dir/ 3\n\n"
        echo "Aclaraciones:"
        echo " - Es necesario indicar un directorio. Sino el script se detendrá."
        echo " - Las opciones y la frecuencia son opcionales."
        echo -e " - Si no selecciona frecuencia, se programará por defecto de forma\n semanal los sábados a las 23:50 hs."
        exit 0
}

# Se define la función frec. Con la selección del usuario, se determinará la frecuencia en la que se schedulará el backup.
function frec(){
        case $1 in
	# En caso de que el argumento ingresado sea 1, 2, 3, 4 o 5, se definen las frecuencias correspondientes. Si se ingresa otro argumento, se deja mensaje y se devuelve error.
               
       		1)
                        # Una vez por día, todos los días, a las 23:50 hs.
			frecuencia="50 23 * * *"
                        ;;

                2)
                        # Una vez por día, todos los días, a las 00:10 hs.
			frecuencia="10 0 * * *"
                        ;;

                3)
                        # Una vez por día, de lunes a viernes, a las 23:50 hs.
			frecuencia="50 23 * * 1-5"
                        ;;

                4)
                        # Una vez por día, de lunes a viernes, a las 00:10 hs.
			frecuencia="10 0 * * 1-5"
                        ;;

		5)
                        # Dos veces por día, todos los días, a las 11:50 y 23:50 hs.
			frecuencia="50 11,23 * * *"
                        ;;

                *)
                        # Cualquier otro valor
			echo "Ingrese un argumento válido"
                        exit 1
                ;;
        esac
}

function keygen(){
	# Se verifica si la llave se encuentra generada a través de la no existencia del fichero. Si no, se genera y se envía.
	if [ ! -f /home/istea/.ssh/id_rsa ]; then
        	ssh-keygen -t rsa
        	ssh-copy-id -i ~/.ssh/id_rsa.pub istea@localhost
	fi
}


# Se recorren los argumentos ingresados.
for argumento in "${!args[@]}"; do

        case ${args[$argumento]} in
	        
		# Si el argumento ingresado es --HELP, --help o --Help, se llama a la función help. Flag de control se establece en 1.
		--HELP|--help|--Help)
			flag=1
			help
                        ;;
                
		# Si el argumento ingresado inicia con guión, el valor se guarda en la variable opciones.
		-*)
		        opciones=${args[$argumento]}
                        ;;
                
		# Si el argumento ingresado finaliza con /, el valor se guarda en la variable directorio. Se verifica si lo ingresado es un directorio existente. 
		# En ese caso, flag de control se establece en 1.
		*/)
			directorio=${args[$argumento]}
			
			if [ -d "$directorio" ]; then
   				flag=1
			else
				echo "Debe ingresar un directorio válido"
				exit 1
			fi
                        ;;
                
		# Si el argumento está entre 1 y 5, se llama a la función frec con el valor ingresado como argumento.
		[1-5])
                        frec "${args[$argumento]}"
                        ;;
                
		# Si se ingresa cualquier otro argumento, se deja mensaje y se devuelve error.
		*)
                        echo "Error. No ingresó argumento válido. Ejecute el script nuevamente"
                        exit 1
                        ;;
        esac
done

# Se verifica si el flag es cero. Si lo es, se deja un mensaje y el script finaliza.
if [[ $flag -eq 0 ]]; then
        echo "Debe ingresar un directorio válido para poder efectuar el backup. Ejecute el script nuevamente"
        exit 1
fi

# Llamado a función keygen
keygen

# Se efectúa el backup del directorio utilizando las opciones también enviadas por parámetro. El directorio de backup se crea si no existe.
rsync $opciones $directorio istea@localhost:/tmp/backups.bak

# Se verifica si la automatización que se pretende configurar ya existe.
# Se define variable registro para facilitar la búsqueda.
registro=$frecuencia" /backup.sh "$directorio
   	
if ! crontab -l | fgrep "$registro"; then
	# Si el registro no existe, se incorpora en la lista e imprime mensaje de éxito.
	#(crontab -l ; echo "$frecuencia /backup.sh $directorio") | crontab -
	(crontab -l ; echo "$registro") | crontab -
	echo "Automatización incorporada: Directorio: $directorio - Frecuencia: $frecuencia"
else
	# Si el registro existe, no se incorpora y notifica con un mensaje.
	echo "Automatización existente: Directorio: $directorio - Frecuencia: $frecuencia"

fi
