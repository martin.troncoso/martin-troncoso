#!/bin/bash

#Se solicita al usuario el ingreso del nombre del archivo
read -p "Introduzca el nombre del fichero: " file

#Verifica si el usuario no ingresa un nombre
if [ "$file" != "" ]; then

		#Comprueba existencia de fichero
        if [ -f $file ]; then
                echo "El fichero $file existe."
				
				#Verifica permisos de ejecución 
                if [ -x $file ];then
                        echo "Tiene permisos de ejecución sobre el fichero $file"
                        ls -l $file
						
						#Consulta ejecución de fichero 
                        read -p "¿Desea ejecutar $file? Y/N:" ans
						
						#Verifica si la acción ingresada es Y/N considerando minúsculas
                        if [[ $ans =~ [Yy] ]]; then
                                echo "Ejecutando fichero $file......."
                                exit
						elif
                                [[ $ans =~ [Nn] ]]; then
                                echo "No se ejecutará el fichero $file"
                                exit
                        else
                                [[ $ans =~ [*] ]]
                                echo "¡Respuesta no válida! Ejecute nuevamente"
                                exit

                        fi
				#Verifica si el fichero tiene permisos de ejecución 
				else
					[ ! -x $file ]; echo "Pero no tiene permisos de ejecución sobre el fichero $file"
					
					#Consulta si se le otorgan permisos
					read -p "¿Desea otorgar permisos a $file? Y/N:" ans1

					#Verifica si la acción ingresada es Y/N considerando minúsculas
					if [[ $ans1 =~ [Yy] ]]; then
                        chmod +x $file
                        echo "¡Permisos otorgados sobre $file!"
                        ls -l $file
					elif
                        [[ $ans1 =~ [Nn] ]]; then
                        echo "No se otorgarán permisos sobre $file"
					else
                        [[ $ans1 =~ [*] ]]
                        echo "¡Respuesta no válida! Ejecute nuevamente"
                        exit
					fi
				fi
		
        else [ ! -f $file ]
                echo "El fichero ingresado no existe"
                exit
        fi

else
        echo "No ha ingresado el nombre del fichero. Ejecute nuevamente"
fi
